"use strict";
const boom = require('boom');
const axios = require('axios');
const instance = axios.create({
	baseURL: process.env.MC_API_URL,
	timeout: 1000,
	headers: {'Authorization': `Bearer ${process.env.MC_API_KEY}`}
});

const plaid_update_url = process.env.PLAID_UPDATE_URL;
const emoji = require('node-emoji');

module.exports = (logger) => {
	return {
		sendItemLoginRequiredMessage: async (item, user) => {
			let bodyParameters = {
				subscriber_id: parseInt(user.id),
				data: {
					version: "v2",
					content: {
						messages: [ 
							{
								"type": "text",
								"text": `Hmmm ${user.first_name}, I can't seem to connect to your ${item.institution_name} account.`
							},
							{
								"type": "text",
								"text": `Not to worry though ${emoji.get('sun_with_face')}. You can fix this by re-entering the most current login credentials.`,
								"buttons": [
									{
										"type": "url",
										"caption": `Update Card`,
										"url": `${plaid_update_url}/${user.id}/${user.many_chat_auth_token}/item/${item.id}`
									}
								]
							}
						],
					}
				}
			}
			return instance.post(`/fb/sending/sendContent`, bodyParameters).catch((err) => { 
				logger.error(`Error occured whilst tring to use ManyChat API _puid:${user.id}`);
				//throw boom.internal("ManyChat API Error")
			} );
		}
	};
}