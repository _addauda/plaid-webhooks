"use strict";
const format = require('pg-format');
const boom = require('boom');

// Defines helper functions for saving and getting tweets, using the database `pool`
module.exports = (pool) => {
	return {
		verifyUserCredentials: (_puid, _mcat) => {
			let queryString = format("SELECT id FROM users WHERE id = %L AND many_chat_auth_token = %L LIMIT 1", _puid, _mcat)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		verifyItem: (item_id) => {
			let queryString = format("SELECT id FROM items WHERE id = %L LIMIT 1", item_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getItemById: (_item_id) => {
			let queryString = format("SELECT items.id, items.access_token, items.institution_name, items.user_id FROM items WHERE id = %L LIMIT 1", _item_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getUserById: (_puid) => {
			let queryString = format("SELECT users.id, users.first_name, users.many_chat_auth_token FROM users WHERE id = %L LIMIT 1", _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		updateItemStatus: (_item_id, status) => {
			let queryString = format("UPDATE items SET status = %L WHERE id = %L", status, _item_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		insertTransactions: (plaidTransactions) => {
			let queryString = format("INSERT INTO transactions (id, name, iso_currency_code, amount, date, type, pending, category_id, pending_transaction_id, item_id, category) VALUES %L ON CONFLICT (id) DO NOTHING", plaidTransactions)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		}
	};
}