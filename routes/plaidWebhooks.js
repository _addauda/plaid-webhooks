"use strict";

const express = require('express');
const router = express.Router();
const Validator = require('node-input-validator');
const moment = require('moment');
const now = moment();
const today = now.format('YYYY-MM-DD');
const daysAgo = now.subtract(process.env.PLAID_ROLLING_INTERVAL ? process.env.PLAID_ROLLING_INTERVAL : 2, 'days').format('YYYY-MM-DD');
const boom = require('boom');

//formats plaid categories in transactions into comma seperated string
const formatCategoryString = (category_array) => {
	return `{${category_array.map(category => `"${category}"`).join(',')}}`;
}

//formats plaid transactions into an array for sql insert
const formatPlaidTransactions = (item_id, transactions) => {
	let plaidTransactions = []
	for (let transaction of transactions) {
		let transactionArray = [transaction.transaction_id, transaction.name, transaction.iso_currency_code, transaction.amount, transaction.date, transaction.transaction_type, transaction.pending, transaction.category_id, transaction.pending_transaction_id, item_id, formatCategoryString(transaction.category)]
		plaidTransactions.push(transactionArray)
	}
	return plaidTransactions;
}

module.exports = (asyncMiddleware, plaidClient, dbHelpers, mcHelpers, logger) => {

	router.post('/', asyncMiddleware(async (req, res) => {

		let validator = new Validator(req.body, {
			'item_id': 'required|string',
			'webhook_code': 'required|string',
			'webhook_type': 'required|string',
		});

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed _puid:${req.body._puid} with errors ${JSON.stringify(validator.errors)}`);
		}

		let getItemResult = await dbHelpers.getItemById(req.body.item_id);
		if (getItemResult.rowCount === 0) {
			throw boom.notFound(`failed to find item for item_id:${req.body.item_id}`);
		}

		switch(req.body.webhook_type) {
			case "TRANSACTIONS":
				switch(req.body.webhook_code) {
					case "INITIAL_UPDATE":
					case "HISTORICAL_UPDATE":
					case "DEFAULT_UPDATE":

						let plaidResult = await plaidClient.getTransactions(getItemResult.rows[0].access_token, daysAgo, today);
						let plaidTransactions = formatPlaidTransactions(req.body.item_id, plaidResult.transactions);
						let insertTransactionsResult = await dbHelpers.insertTransactions(plaidTransactions);
						
						logger.info(`received webhook with info ${JSON.stringify(req.body)}`)
						logger.info(`inserted ${insertTransactionsResult.rowCount} transactions webhook:${req.body.webhook_code}`);
						res.sendStatus(200);
						break;
					default:
						logger.warn(`No handler defined for webhook_type:${req.body.webhook_type} webhook_code:${req.body.webhook_code}`);
						res.sendStatus(200);
				}
				break;
			case "ITEM":
				switch(req.body.webhook_code) {
					case "ERROR":
						let item_status = "ITEM_LOGIN_REQUIRED";
						let updateItemResult = await dbHelpers.updateItemStatus(req.body.item_id, item_status);
						logger.warn(`item_id:${req.body.item_id} is in status:${item_status} with error ${JSON.stringify(req.body.error)}`);
						res.sendStatus(200);


						let getUserResult = await dbHelpers.getUserById(getItemResult.rows[0].user_id);

						if (getUserResult.rowCount) {
							let httpResult = await mcHelpers.sendItemLoginRequiredMessage(getItemResult.rows[0], getUserResult.rows[0]);
							logger.info(`sent ${item_status} notification to user ${getItemResult.rows[0].user_id}`);
						}

						break;
					default:
						logger.warn(`No handler defined for webhook_type:${req.body.webhook_type} webhook_code:${req.body.webhook_code}`);
						res.sendStatus(200);
				}
				break;
			default:
				logger.warn(`No handler defined for webhook_type:${req.body.webhook_type}`);
				res.sendStatus(200);
		}
	}));

	return router;
}