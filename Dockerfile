# Base Image
FROM node:10

# Create app directory
WORKDIR /usr/src/app

# Make user 'node' owner of folder
RUN chown -R node:node /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

USER node

# RUN npm install
# If you are building your code for production
RUN npm ci --only=production

# Copy source files to container with node as owner
COPY --chown=node:node ./helpers/ ./helpers/
COPY --chown=node:node ./routes/ ./routes/
COPY --chown=node:node ./index.js ./

# Confirm that all files are copied
RUN ls -la ./*

# Expose application port
EXPOSE 8080

# Run as non-root user
USER node

# Start command
CMD [ "node", "index.js" ]