'use strict';
require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const boom = require('boom');

const plaid = require('plaid');
const plaidClient = new plaid.Client(
	process.env.PLAID_CLIENT_ID,
	process.env.PLAID_SECRET,
	process.env.PLAID_PUBLIC_KEY,
	(process.env.PLAID_ENV === "production") ? plaid.environments.production
	: (process.env.PLAID_ENV === "development") ? plaid.environments.development : plaid.environments.sandbox
);

const Pool = require('pg').Pool
const pool = new Pool({
	host: process.env.DB_HOST,
	database: process.env.DB_NAME,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	port: process.env.DB_PORT,
});

//log app events
const log4js = require('log4js');
log4js.configure(
	{
	  appenders: {
		file: {
		  type: 'file',
		  filename: `app.log`,
		  maxLogSize: 10 * 1024 * 1024, // = 10Mb
		  backups: 5, // keep five backup files
		  compress: true, // compress the backups
		  encoding: 'utf-8',
		  mode: 0o0640,
		  flags: 'w+'
		},
		out: {
		  type: 'stdout'
		}
	  },
	  categories: {
		default: { appenders: ['file', 'out'], level: 'trace' }
	  }
	}
);

const logger = log4js.getLogger();

// Constants
const PORT = process.env.PORT;
const HOST = process.env.HOST;

const obfuscate = (urlToObfuscate) => {
	return urlToObfuscate.replace(/[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i,  '$REDACTED$');
};

// App
const app = express();

//log app requests
app.use(morgan((tokens, request, response) => {
		return [
			tokens['remote-addr'](request, response), '-', '-',
			tokens['remote-user'](request, response,), '[',
			tokens['date'](request, response), ']', 
			'"', tokens.method(request, response),
			obfuscate(tokens.url(request, response)), 'HTTP/',
			tokens['http-version'](request, response), '"',
			tokens.status(request, response),
			tokens.res(request, response, 'content-length'), '-',
			tokens['response-time'](request, response), 'ms'
		].join(' ')
	},{
	stream: fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
	}
));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Custom

//async middleware for uniform error handling
const asyncMiddleware = fn => (req, res, next) => {
	Promise.resolve(fn(req, res, next)).catch((err) => {
		if (!err.isBoom) {
			err = boom.badImplementation(err);
			res.sendStatus(err.output.payload.statusCode);
			next(err);
		}
		logger.error(err.output.payload.statusCode, err.output.payload.message);
		res.sendStatus(err.output.payload.statusCode);
		next(err);
	});
};

const dbHelpers = require("./helpers/db-helpers.js")(pool);
const mcHelpers = require("./helpers/mc.js")(logger);
//middleware verify whether item_id is for a valid user
// const verifyItemIdMiddleWare = (req, res, next) => {
// 	if(req.path === "/") {
// 		next(); 
// 	} else {
// 		dbHelpers.verifyItem(req.body.item_id, (error, result) => {
// 			if(error) {
// 				logger.error(`failed to execute query to find item_id:${req.body.item_id} path:${req.path}`);
// 				logger.error(error.message);
// 				res.sendStatus(500);
// 			} else {
// 				if(result) {
// 					next();
// 				} else {
// 					logger.error(`failed to find item_id:${req.body.item_id} path:${req.path}`);
// 					res.sendStatus(401);
// 				};
// 			}
// 		});
// 	} 
// }

// app.use(verifyItemIdMiddleWare);

// Routes
const plaidWebhookRoutes = require("./routes/plaidWebhooks.js")(asyncMiddleware, plaidClient, dbHelpers, mcHelpers, logger);

app.use("/services/plaid/webhooks", plaidWebhookRoutes);

app.get('/', (req, res) => {
	res.send({status:"Success", message:"Endpoint is up"});
});

app.listen(PORT, HOST);
logger.info(`${process.env.SERVICE_NAME} is running on http://${HOST}:${PORT}`);